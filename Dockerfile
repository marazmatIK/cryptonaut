FROM openjdk:8u171-alpine3.7
RUN apk --no-cache add curl
COPY target/cryptonaut*.jar cryptonaut.jar

EXPOSE 8080 8081

CMD java ${JAVA_OPTS} -jar cryptonaut.jar