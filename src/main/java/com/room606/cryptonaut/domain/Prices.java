package com.room606.cryptonaut.domain;

import java.util.List;

public class Prices {
    private List<Price> prices;
    private String fiatCurrency;

    public Prices(List<Price> prices, String fiatCurrency) {
        this.prices = prices;
        this.fiatCurrency = fiatCurrency;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public String getFiatCurrency() {
        return fiatCurrency;
    }

    public void setFiatCurrency(String fiatCurrency) {
        this.fiatCurrency = fiatCurrency;
    }
}
