package com.room606.cryptonaut.domain;

import java.math.BigDecimal;

public class Total {
    private String fiatCurrency;

    private Portfolio portfolio;
    private BigDecimal value;

    public Total() {
    }

    public Total(String fiatCurrency, Portfolio coins, BigDecimal value) {
        this.fiatCurrency = fiatCurrency;
        this.portfolio = coins;
        this.value = value;
    }

    public String getFiatCurrency() {
        return fiatCurrency;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setFiatCurrency(String fiatCurrency) {
        this.fiatCurrency = fiatCurrency;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
