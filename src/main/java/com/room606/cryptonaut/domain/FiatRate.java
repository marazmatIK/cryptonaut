package com.room606.cryptonaut.domain;

import java.math.BigDecimal;

public class FiatRate {
    private String base;
    private String counter;
    private BigDecimal value;

    public FiatRate(String base, String counter, BigDecimal value) {
        this.base = base;
        this.counter = counter;
        this.value = value;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
