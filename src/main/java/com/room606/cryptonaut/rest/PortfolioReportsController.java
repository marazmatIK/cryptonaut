package com.room606.cryptonaut.rest;

import com.room606.cryptonaut.PortfolioService;
import com.room606.cryptonaut.domain.Total;
import com.room606.cryptonaut.markets.FiatExchangeRatesService;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import java.math.RoundingMode;

@Controller("/cryptonaut/restapi/")
public class PortfolioReportsController {

    private final CryptoMarketDataService cryptoMarketDataService;
    private final FiatExchangeRatesService fiatExchangeRatesService;
    private final PortfolioService portfolioService;

    public PortfolioReportsController(CryptoMarketDataService cryptoMarketDataService, FiatExchangeRatesService fiatExchangeRatesService, PortfolioService portfolioService) {
        this.cryptoMarketDataService = cryptoMarketDataService;
        this.fiatExchangeRatesService = fiatExchangeRatesService;
        this.portfolioService = portfolioService;
    }

    @Get("/portfolio/total")
    @Consumes({MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Produces(MediaType.TEXT_PLAIN)
    public String totalValue(@QueryValue("fiatCurrency") String fiatCurrency) {
        return portfolioService.calculateTotalValue(portfolioService.loadPortfolio(), fiatCurrency)
                .get().setScale(2, RoundingMode.CEILING).toString();
    }

    @Get("/portfolio/total.json")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Total totalValueAsJson(@QueryValue("fiatCurrency") String fiatCurrency) {
        return new Total(fiatCurrency, portfolioService.loadPortfolio(),
                portfolioService.calculateTotalValue(portfolioService.loadPortfolio(), fiatCurrency)
                        .get().setScale(2, RoundingMode.CEILING));
    }
}
