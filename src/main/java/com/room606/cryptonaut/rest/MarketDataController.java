package com.room606.cryptonaut.rest;

import com.room606.cryptonaut.domain.Price;
import com.room606.cryptonaut.domain.Prices;
import com.room606.cryptonaut.markets.FiatExchangeRatesService;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller("/cryptonaut/restapi/")
public class MarketDataController {

    private final CryptoMarketDataService cryptoMarketDataService;
    private final FiatExchangeRatesService fiatExchangeRatesService;

    public MarketDataController(CryptoMarketDataService cryptoMarketDataService, FiatExchangeRatesService fiatExchangeRatesService) {
        this.cryptoMarketDataService = cryptoMarketDataService;
        this.fiatExchangeRatesService = fiatExchangeRatesService;
    }

    @Get("/prices")
    @Consumes({MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Produces(MediaType.TEXT_PLAIN)
    public String pricesAsText(@QueryValue("coins") String[] coins, @QueryValue("fiatCurrency") String fiatCurrency) {
        return "Prices, for " + Stream.of(coins).map(coin -> coin + "/" + fiatCurrency + ": " + cryptoMarketDataService.getPrice(coin, fiatCurrency)).collect(Collectors.joining(", "));
    }

    @Get("/prices.json")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Prices pricesAsJson(@QueryValue("coins") String[] coins, @QueryValue("fiatCurrency") String fiatCurrency) {
        return getPrices(coins, fiatCurrency);
    }

    @Get("/fiat/{base}/{counter}")
    @Consumes({MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Produces(MediaType.TEXT_PLAIN)
    public String fiatRate(String base, String counter) {
        return "Prices, for " + base + "/" + counter + ": " + fiatExchangeRatesService.getFiatPrice(base, counter);
    }

    @Get("/fiat/{base}/{counter}/json")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public String fiatRateAsJson(String base, String counter) {
        return String.format("{\"base\":\"%s\",\"counter\": \"%s\", \"price\":\"%s\"}", base, counter,
                fiatExchangeRatesService.getFiatPrice(base, counter));
    }

    private Prices getPrices(String[] coins, String fiatCurrency) {
        List<Price> prices = Stream.of(coins)
                .map(coin -> new Price(coin, cryptoMarketDataService.getPrice(coin, fiatCurrency)))
                .collect(Collectors.toList());
        return new Prices(prices, fiatCurrency);
    }
}
