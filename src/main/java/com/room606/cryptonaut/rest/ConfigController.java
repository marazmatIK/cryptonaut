package com.room606.cryptonaut.rest;

import com.room606.cryptonaut.PortfolioService;
import com.room606.cryptonaut.domain.Portfolio;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import javax.inject.Inject;

@Controller("/cryptonaut/restapi/")
public class ConfigController {

    @Inject
    private PortfolioService portfolioService;

    @Post("/portfolio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Portfolio savePortfolio(@Body Portfolio portfolio) {
        return portfolioService.savePortfolio(portfolio);
    }

    @Get("/portfolio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Portfolio loadPortfolio() {
        return portfolioService.loadPortfolio();
    }
}
