package com.room606.cryptonaut;

import com.room606.cryptonaut.domain.Portfolio;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import io.micronaut.context.annotation.Requires;
import io.reactiverse.pgclient.data.Numeric;
import io.reactiverse.reactivex.pgclient.*;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Requires(notEnv="test")
public class PortfolioServiceImpl implements PortfolioService {

    private final PgPool pgPool;
    private final CryptoMarketDataService cryptoMarketDataService;

    private static final String UPDATE_COIN_AMT = "INSERT INTO portfolio (coin, amount) VALUES ($1, $2) ON CONFLICT (coin) " +
            "DO UPDATE SET amount = $3";
    private static final String DELETE_COIN_AMT = "DELETE FROM portfolio WHERE coin NOT IN (%s)";

    private static final String SELECT_COINS_AMTS = "SELECT coin, amount FROM portfolio";

    @Inject
    public PortfolioServiceImpl(PgPool pgPool, CryptoMarketDataService cryptoMarketDataService) {
        this.pgPool = pgPool;
        this.cryptoMarketDataService = cryptoMarketDataService;
    }

    //TODO: Make it transactional
    //@Transactional
    public Portfolio savePortfolio(Portfolio portfolio) {
        List<Tuple> records = portfolio.getCoins()
                .entrySet()
                .stream()
                .map(entry -> Tuple.of(entry.getKey(), Numeric.create(entry.getValue()), Numeric.create(entry.getValue())))
                .collect(Collectors.toList());

        pgPool.preparedBatch(UPDATE_COIN_AMT, records, pgRowSetAsyncResult -> {
            if (!pgRowSetAsyncResult.succeeded()) {
                pgRowSetAsyncResult.cause().printStackTrace();
            }
        }).rxBegin().blockingGet();

        if (portfolio.getCoins() != null && !portfolio.getCoins().isEmpty()) {
            String coinCodes = portfolio.getCoins().keySet().stream().map(coin -> "'" + coin + "'").collect(Collectors.joining(","));
            pgPool.rxQuery(String.format(DELETE_COIN_AMT, coinCodes)).blockingGet();
        }


        /*pgPool.getConnection(res -> {
            if (res.succeeded()) {

                // Transaction must use a connection
                PgConnection conn = res.result();

                // Begin the transaction
                PgTransaction tx = conn.begin();

                // Various statements
                conn.rxPreparedBatch(UPDATE_COIN_AMT, records).blockingGet();
                *//*if (portfolio.getCoins() != null && !portfolio.getCoins().isEmpty()) {
                    String coinCodes = portfolio.getCoins().keySet().stream().collect(Collectors.joining(","));
                    System.out.println(coinCodes);
                    conn.preparedBatch(DELETE_COIN_AMT, Collections.singletonList(Tuple.of(coinCodes)), pgRowSetAsyncResult -> {
                        pgRowSetAsyncResult.cause().printStackTrace();
                    });
                }*//*

                // Commit the transaction
                tx.commit(ar -> {
                    if (ar.succeeded()) {
                        System.out.println("Transaction succeeded");
                    } else {
                        System.out.println("Transaction failed " + ar.cause().getMessage());
                    }

                    // Return the connection to the pool
                    conn.close();
                });
            }
        });*/


        /**/
        return portfolio;
    }

    public Portfolio loadPortfolio() {
        Map<String, BigDecimal> coins = new HashMap<>();
        PgIterator pgIterator = pgPool.rxPreparedQuery(SELECT_COINS_AMTS).blockingGet().iterator();
        while (pgIterator.hasNext()) {
            Row row = pgIterator.next();
            coins.put(row.getString("coin"), new BigDecimal(row.getValue("amount").toString()));
        }

        Portfolio portfolio = new Portfolio();
        portfolio.setCoins(coins);
        return portfolio;
    }

    public Optional<BigDecimal> calculateTotalValue(Portfolio portfolio, String fiatCurrency) {
        return portfolio.getCoins()
                .entrySet()
                .stream()
                .map(coinEntry -> coinEntry.getValue().multiply(cryptoMarketDataService.getPrice(coinEntry.getKey(), fiatCurrency)))
                .reduce(BigDecimal::add);
    }
}
