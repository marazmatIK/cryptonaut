package com.room606.cryptonaut.markets;

import org.knowm.xchange.service.marketdata.MarketDataService;

public interface MarketDataServiceFactory {
    MarketDataService getMarketDataService();
}
