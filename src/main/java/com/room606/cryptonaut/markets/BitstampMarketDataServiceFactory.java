package com.room606.cryptonaut.markets;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.service.marketdata.MarketDataService;

import javax.inject.Singleton;

@Singleton
public class BitstampMarketDataServiceFactory implements MarketDataServiceFactory {

    private Exchange bitstamp = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());

    private MarketDataService marketDataService = bitstamp.getMarketDataService();

    @Override
    public MarketDataService getMarketDataService() {
        return marketDataService;
    }
}
