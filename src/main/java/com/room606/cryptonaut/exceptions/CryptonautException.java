package com.room606.cryptonaut.exceptions;

public class CryptonautException extends RuntimeException {
    public CryptonautException(String message) {
        super(message);
    }
    public CryptonautException(String message, Throwable cause) {
        super(message, cause);
    }
}
