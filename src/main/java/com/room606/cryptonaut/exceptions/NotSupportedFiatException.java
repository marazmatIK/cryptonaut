package com.room606.cryptonaut.exceptions;

public class NotSupportedFiatException extends CryptonautException {
    public NotSupportedFiatException(String message) {
        super(message);
    }

    public NotSupportedFiatException(String message, Throwable cause) {
        super(message, cause);
    }
}
