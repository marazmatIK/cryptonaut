package com.room606.cryptonaut.rest;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;

import static org.junit.Assert.assertEquals;

public class FiatRatesControllerTest extends TestBase {

    @ClassRule
    public static GenericContainer redis = new FixedHostPortGenericContainer("redis")
            .withFixedExposedPort(6379, 6379);

    @Test
    public void shouldGetRates() {
        HttpResponse<Object> response = client.toBlocking().exchange("/cryptonaut/restapi/fiat/USD/RUR");
        assertEquals(response.status(), HttpStatus.OK);
    }
}