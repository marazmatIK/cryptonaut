package com.room606.cryptonaut;

import com.room606.cryptonaut.exceptions.CryptonautException;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import com.room606.cryptonaut.markets.FiatExchangeRatesService;
import com.room606.cryptonaut.markets.MarketDataServiceFactory;
import org.junit.Test;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.exceptions.CurrencyPairNotValidException;
import org.knowm.xchange.service.marketdata.MarketDataService;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MarketDataServiceTest {

    private final FiatExchangeRatesService fiatExchangeRatesService = mock(FiatExchangeRatesService.class);
    private final MarketDataServiceFactory marketDataServiceFactory = mock(MarketDataServiceFactory.class);
    private final MarketDataService marketDataService = mock(MarketDataService.class);

    @Test
    public void priceForUsdNoDirectRate() throws IOException {
        when(marketDataServiceFactory.getMarketDataService()).thenReturn(marketDataService);
        String coinCode = "ETH";
        String fiatCurrencyCode = "USD";
        String baseCurrencyCode = "BTC";

        BigDecimal priceA = new BigDecimal("6500.00");
        Ticker btcUsdTicker = new Ticker.Builder().bid(priceA).build();

        BigDecimal priceB = new BigDecimal("0.034");
        Ticker targetCoinBaseCryptoTicker = new Ticker.Builder().bid(priceB).build();

        when(marketDataService.getTicker(new CurrencyPair(new Currency(baseCurrencyCode), new Currency(fiatCurrencyCode)))).thenReturn(btcUsdTicker);
        when(marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(baseCurrencyCode)))).thenReturn(targetCoinBaseCryptoTicker);
        when(marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(fiatCurrencyCode)))).thenThrow(new CurrencyPairNotValidException());

        CryptoMarketDataService cryptoMarketDataService = new CryptoMarketDataService(fiatExchangeRatesService, marketDataServiceFactory);
        assertEquals(priceA.multiply(priceB), cryptoMarketDataService.getPrice(coinCode, fiatCurrencyCode));
    }

    @Test
    public void priceForUsdDirectRate() throws IOException {
        when(marketDataServiceFactory.getMarketDataService()).thenReturn(marketDataService);
        String coinCode = "ETH";
        String fiatCurrencyCode = "USD";

        BigDecimal priceA = new BigDecimal("218.58");
        Ticker targetTicker = new Ticker.Builder().bid(priceA).build();
        when(marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(fiatCurrencyCode)))).thenReturn(targetTicker);

        CryptoMarketDataService cryptoMarketDataService = new CryptoMarketDataService(fiatExchangeRatesService, marketDataServiceFactory);
        assertEquals(priceA, cryptoMarketDataService.getPrice(coinCode, fiatCurrencyCode));
    }

    @Test(expected = CryptonautException.class)
    public void priceForNotExistingRate() throws IOException {
        when(marketDataServiceFactory.getMarketDataService()).thenReturn(marketDataService);
        String coinCode = "XEM";
        String fiatCurrencyCode = "USD";
        String baseCurrencyCode = "BTC";


        when(marketDataService.getTicker(new CurrencyPair(new Currency(baseCurrencyCode), new Currency(fiatCurrencyCode)))).thenThrow(new CurrencyPairNotValidException());
        when(marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(baseCurrencyCode)))).thenThrow(new CurrencyPairNotValidException());
        when(marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(fiatCurrencyCode)))).thenThrow(new CurrencyPairNotValidException());

        CryptoMarketDataService cryptoMarketDataService = new CryptoMarketDataService(fiatExchangeRatesService, marketDataServiceFactory);
        cryptoMarketDataService.getPrice(coinCode, fiatCurrencyCode);
    }
}
