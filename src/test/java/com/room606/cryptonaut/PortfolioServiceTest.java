package com.room606.cryptonaut;

import com.room606.cryptonaut.domain.Portfolio;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import io.reactiverse.reactivex.pgclient.PgPool;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PortfolioServiceTest {

    private PgPool pgPool = mock(PgPool.class);
    private CryptoMarketDataService cryptoMarketDataService = mock(CryptoMarketDataService.class);

    @Test
    public void portfolio() {
        Portfolio portfolio = new Portfolio();
        Map<String, BigDecimal> coins = new HashMap<>();
        coins.put("OMG", new BigDecimal(13.56));
        coins.put("LSK", new BigDecimal(16.007));
        coins.put("BTC", new BigDecimal(0.0005));
        portfolio.setCoins(coins);

        BigDecimal price1 = new BigDecimal(2.25);
        BigDecimal price2 = new BigDecimal(10);
        BigDecimal price3 = new BigDecimal(6500.50);

        String fiat = "USD";

        when(cryptoMarketDataService.getPrice(eq("OMG"), eq(fiat))).thenReturn(price1);
        when(cryptoMarketDataService.getPrice(eq("LSK"), eq(fiat))).thenReturn(price2);
        when(cryptoMarketDataService.getPrice(eq("BTC"), eq(fiat))).thenReturn(price3);

        PortfolioService portfolioService = new PortfolioServiceImpl(pgPool, cryptoMarketDataService);

        BigDecimal total = portfolioService.calculateTotalValue(portfolio, "USD").get();

        assertEquals("193.83", String.format("%.2f", total.floatValue()));
    }

    @Test
    public void emptyPortfolio() {
        Portfolio portfolio = new Portfolio();

        String fiat = "USD";

        when(cryptoMarketDataService.getPrice(eq("OMG"), eq(fiat))).thenReturn(new BigDecimal(2.25));

        PortfolioService portfolioService = new PortfolioServiceImpl(pgPool, cryptoMarketDataService);

        assertFalse(portfolioService.calculateTotalValue(portfolio, "USD").isPresent());
    }
}
