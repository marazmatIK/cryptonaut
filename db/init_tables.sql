CREATE TABLE portfolio (
    id                 serial CONSTRAINT coin_amt_primary_key PRIMARY KEY,
    coin    varchar(16) NOT NULL UNIQUE,
	amount  NUMERIC     NOT NULL
);